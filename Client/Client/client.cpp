#include <iostream>
#include <string>
#include <fstream>
#include <winsock2.h>
#pragma comment (lib, "Ws2_32.lib")

using namespace std;

/*
3.
- nawi�zanie po��czenia przez klienta
- przes�anie przez klienta identyfikatora
- przes�anie przez klienta has�a
- uwierzytelnienie klienta przez serwer na podstawie identyfikatora i has�a
- odes�anie informacji o pozytywnym lub negatywnym wyniku uwierzytelnienia
- roz��czenie zainicjowane przez klienta
Po odebraniu ka�dej komendy przez serwer odes�anie potwierdzenia do klienta.
*/

void readFile(char *&ipaddr, int &port)
{
  std::ifstream h_file;
  std::string data;
  h_file.open("config.txt", std::ios::in);
  if(h_file.good())
  {
    getline(h_file,data);
    std::string sub = data.substr(0, data.find(":"));
    ipaddr = new char[sub.size()+1];
    std::copy(sub.begin(), sub.end(), ipaddr);
    ipaddr[sub.size()] = '\0';
    port = atoi(data.substr(data.find(":")+1, data.length()).c_str());
    h_file.close();
  }
  else
  {
    std::string ipaddress, porter;
    cout << "[CONF]\tProsze podac adres IP: ";
	  cin >> ipaddress;
    cout << "[CONF]\tProsze podac numer portu: ";
	  cin >> porter;
    ipaddr = strdup(ipaddress.c_str());
    port = atoi(porter.c_str());
  }
  cout << "[INFO]\tAdres oraz port ustwiono na: " << ipaddr << ":" << port << endl;
}

int main()
{
	cout << "-----------------------" << endl;
  cout << "         KLIENT         " << endl;
  cout << "-----------------------" << endl;

	//inicjalizujemy Winsock
	WSADATA wsaData;

	int inicjalizacja = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (inicjalizacja != NO_ERROR) cout <<  "[ ERR ]\tBlad inicjalizacji!" << endl;

	//tworzymy gniazdo
	SOCKET hsocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if(hsocket == INVALID_SOCKET){
		cout << "[ ERR ]\tBlad podczas tworzenia gniazdka: " << WSAGetLastError() << "!" << endl;
		WSACleanup();
		return 1;
	}
  cout << "[INFO]\tInicjalizacja poprawna." << endl;

	//uzupeliamy adres IP i portu
	sockaddr_in service;
	memset(&service, 0, sizeof(service));
	service.sin_family = AF_INET;
	char* ip = "";
	int port;
  readFile(ip, port);
  service.sin_addr.s_addr = inet_addr(ip);
	service.sin_port = htons(port);

	//klient: polaczenie sie z sewerem
	if(connect(hsocket, (SOCKADDR *) &service, sizeof(service)) == SOCKET_ERROR){
		cout << "[ ERR ]\tBlad podczas polaczania!" << endl;
		WSACleanup();
		return 1;
	}
	else cout << "[ OK ]\tNawiazanie polaczenia z serwerem." << endl;

	// klient wysyla ID i czeka na ACK
	char wiadomosc_wyslana[32] = "";
	char wiadomosc_przyslana[32] = "";
	cout << "[CONF]\tPodaj ID: ";
	cin >> wiadomosc_wyslana;
	send(hsocket, wiadomosc_wyslana, strlen(wiadomosc_wyslana), 0);
	cout << "[AWAY]\tKlient wyslal ID" << endl;
	recv(hsocket, wiadomosc_przyslana, 32, 0);
	cout << "[ IN ]\tSERWER: " << wiadomosc_przyslana << endl;

	// klient wysyla PASS i czeka na ACK
	memset( wiadomosc_wyslana, 0, sizeof( wiadomosc_wyslana ) );
	memset( wiadomosc_przyslana, 0, sizeof( wiadomosc_przyslana ) );
	cout << "[CONF]\tPodaj PASS: ";
	cin >> wiadomosc_wyslana;
	send(hsocket, wiadomosc_wyslana, strlen(wiadomosc_wyslana), 0);
	cout << "[AWAY]\tKlient wyslal haslo" << endl;
	recv(hsocket, wiadomosc_przyslana, 32, 0);
	cout << "[ IN ]\tSERWER: " << wiadomosc_przyslana << endl;

	// klient odbiera AUTH
	memset( wiadomosc_wyslana, 0, sizeof( wiadomosc_wyslana ) );
	memset( wiadomosc_przyslana, 0, sizeof( wiadomosc_przyslana ) );
	recv(hsocket, wiadomosc_przyslana, 32, 0);
	cout << "[ IN ]\tSERWER: " << wiadomosc_przyslana << endl;
  system("pause");

	// klient wysyla FIN i czeka na ACK
	// wiadomosc_wyslana = 'FIN';
	memset( wiadomosc_wyslana, 0, sizeof( wiadomosc_wyslana ) );
	memset( wiadomosc_przyslana, 0, sizeof( wiadomosc_przyslana ) );
	char fin[32] = "FIN";
	send(hsocket, fin, 4, 0);
	cout << "[AWAY]\tKlient wysyla FIN" << endl;
	recv(hsocket, wiadomosc_przyslana, 32, 0);
	cout << "[ IN ]\tSERWER: " << wiadomosc_przyslana << endl;

  // zamknij polaczenie
	closesocket(hsocket);
  WSACleanup();
	cout << "[INFO]\tKoniec polaczenia." << endl;
	system("pause");
}