/*
Client app for networking
*/

#include <iostream>
#include <string>
#include "Network.h"

void main()
{
	std::cout << "---------------------------------" << std::endl;
	std::cout << "              Client              " << std::endl;
	std::cout << "---------------------------------" << std::endl;
  
	TsNetwork network;
  char* ip = "";
  int port;
	readFile(ip, port);
    
  try
  {
    network.initWinsock();
    network.initSocket();
    network.setIPAndPort(ip,port);
    network.assignIPAndPort();

    network.Cconnect();
  }
  catch(...)
  {
    log("CLIENT TERMINATED WITH ERRORS");
    system("PAUSE");
  }
}