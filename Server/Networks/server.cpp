#include <iostream>
#include <string>
#include <fstream>
#include <winsock2.h>
#pragma comment (lib, "Ws2_32.lib")

using namespace std;

/*
3.
- nawi�zanie po��czenia przez klienta
- przes�anie przez klienta identyfikatora
- przes�anie przez klienta has�a
- uwierzytelnienie klienta przez serwer na podstawie identyfikatora i has�a
- odes�anie informacji o pozytywnym lub negatywnym wyniku uwierzytelnienia
- roz��czenie zainicjowane przez klienta
Po odebraniu ka�dej komendy przez serwer odes�anie potwierdzenia do klienta.
*/

void readFile(char *&ipaddr, int &port)
{
  std::ifstream h_file;
  std::string data;
  h_file.open("config.txt", std::ios::in);
  if(h_file.good())
  {
    getline(h_file,data);
    std::string sub = data.substr(0, data.find(":"));
    ipaddr = new char[sub.size()+1];
    std::copy(sub.begin(), sub.end(), ipaddr);
    ipaddr[sub.size()] = '\0';
    port = atoi(data.substr(data.find(":")+1, data.length()).c_str());
    h_file.close();
  }
  else
  {
    std::string ipaddress, porter;
    cout << "[CONF]\tProsze podac adres IP: ";
	  cin >> ipaddress;
    cout << "[CONF]\tProsze podac numer portu: ";
	  cin >> porter;
    ipaddr = strdup(ipaddress.c_str());
    port = atoi(porter.c_str());
  }
  cout << "[INFO]\tAdres oraz port ustwiono na: " << ipaddr << ":" << port << endl;
}

int main()
{
	cout << "-----------------------" << endl;
  cout << "         SERWER         " << endl;
  cout << "-----------------------" << endl;

	//inicjalizujemy Winsock
	WSADATA wsaData;

	int inicjalizacja = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (inicjalizacja != NO_ERROR) cout <<  "[ ERR ]\tBlad inicjalizacji Winsock!" << endl;

	//tworzymy gniazdo
	SOCKET hsocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if(hsocket == INVALID_SOCKET){
		cout << "[ ERR ]\tBlad podczas tworzenia gniazdka: " << WSAGetLastError() << "!" << endl;
		WSACleanup();
		return 1;
	}
  cout << "[INFO]\tInicjalizacja poprawna." << endl;

	//uzupeliamy adres IP i portu
	sockaddr_in service;
	memset(&service, 0, sizeof(service));
	service.sin_family = AF_INET;
	char* ip = "";
	int port;
  readFile(ip, port);
	service.sin_addr.s_addr = inet_addr(ip);
	service.sin_port = htons(port);

	//serwer: przypisanie gniazda do adresu
	if(bind(hsocket,(SOCKADDR *) &service, sizeof(service)) == SOCKET_ERROR){
		cout << "[ ERR ]\tBlad podczas przypisywania gniazda do adresu!" << endl;
		closesocket(hsocket);
		return 1;
	}

	//serwer: nasluchiwanie na porcie
	if(listen(hsocket, 1) == SOCKET_ERROR) cout << "[ ERR ]\tBlad podczas nasluchiwania na porcie!" << endl;

	SOCKET hsocket_OK = SOCKET_ERROR;
	cout << "[INFO]\tCzekanie na klienta..." << endl;

	while(hsocket_OK == SOCKET_ERROR){
		hsocket_OK = accept(hsocket, NULL, NULL);
	}

	cout << "[ OK ]\tKlient podlaczony!" << endl;
	hsocket = hsocket_OK;


	//char wiadomosc_wyslana[32] = "";
	char wiadomosc_przyslana[32] = "";
  char id_tmp[32] = "";
	// odbierz ID i potwierdz ACK
	recv(hsocket, id_tmp, 32, 0);
	cout << "[ IN ]\tOtrzymano ID: " << id_tmp << endl;
	send(hsocket, "ACK ID", 6, 0);
	cout << "[AWAY]\tSERWER: ACK" << endl;

	// odbierz PASS i potwierdz ACK:
	memset( wiadomosc_przyslana, 0, sizeof( wiadomosc_przyslana ) );
	recv(hsocket, wiadomosc_przyslana, 32, 0);
	cout << "[ IN ]\tOtrzymano PASS: " << wiadomosc_przyslana << endl;
	send(hsocket, "ACK PASS", 8, 0);
	cout << "[AWAY]\tSERWER: ACK" << endl;


	// wyslij AUTH
  char id[32] = "13";
	char haslo[32] = "tslab";
	if(!strcmp(haslo, wiadomosc_przyslana) && !strcmp(id,id_tmp)){
		send(hsocket, "AUTH OK", 8, 0);
		cout << "[ OK ]\tAutoryzacja poprawna" << endl;
	}
	else {
		send(hsocket, "AUTH FAIL", 9, 0);
		cout << "[ ERR ]\tBlad autoryzacji" << endl;
	}

	// odbieraj az do FIN i wyslij ACK
	char fin[32] = "FIN";
	while(1){
		memset( wiadomosc_przyslana, 0, sizeof( wiadomosc_przyslana ) );
		recv(hsocket, wiadomosc_przyslana, 32, 0);
    cout << "[ IN ]\tKlient: " << wiadomosc_przyslana << endl;
		if(!strcmp(fin, wiadomosc_przyslana)){
			//cout << "[ IN ] Klient: FIN" << endl;
			send(hsocket, "ACK", 4, 0);
			cout << "[AWAY]\tSERWER: ACK" << endl;
			break;
		}
	}

	closesocket(hsocket);
  WSACleanup();
	cout << "[INFO]\tKoniec polaczenia." << endl;
	system("pause");
}